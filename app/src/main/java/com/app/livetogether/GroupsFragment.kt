package com.app.livetogether

import android.app.AlertDialog
import android.app.ProgressDialog
import android.support.v4.app.Fragment
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.text.Html
import android.text.InputType
import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.ExpandableListView
import android.widget.Toast
import kotlin.collections.ArrayList


class GroupsFragment : Fragment() {
    lateinit var myView: View
    lateinit var adapter: GroupsExpandableListAdapter
    lateinit var groupsList: ArrayList<DatabaseHelper.Group>
    lateinit var loadingDialog: ProgressDialog

    companion object {
        fun newInstance(): GroupsFragment {
            return GroupsFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        myView = inflater.inflate(R.layout.fragment_groups, container, false)
        adapter = GroupsExpandableListAdapter(this, context!!, ArrayList())
        loadingDialog = Utils.setupLoadingDialog(context!!)
        val groupsListView = myView.findViewById<ExpandableListView>(R.id.groupsListView)
        groupsListView.setAdapter(adapter)
        myView.findViewById<FloatingActionButton>(R.id.addGroupsButton).setOnClickListener {
            showAddGroupDialog()
        }
        fetchGroups()
        return myView
    }

    private fun showAddGroupDialog() {
        val editText = EditText(context)
        editText.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS

        val dialog = AlertDialog.Builder(context)
            .setView(editText)
            .setTitle(R.string.add_group)
            .setIcon(R.drawable.ic_add_group)
            .setNegativeButton(R.string.Cancel) { _, _ -> }
            .setPositiveButton(R.string.Confirm) { _, _ -> }
            .show()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (editText.text.toString().isBlank()) {
                editText.error = "Group name cannot be empty!"
            } else {
                Utils.startLoading(loadingDialog)
                DatabaseHelper.addGroupRaw(editText.text.toString())
                    .addOnSuccessListener {
                        Log.d(Utils.DEBUG_DB_GROUP, "Successfully added new groupId: ${editText.text}.")
                        dialog.dismiss()
                        fetchGroups()
                    }
                    .addOnFailureListener { exception ->
                        Log.e(Utils.DEBUG_DB_GROUP, "Failure on adding new groupId: ${editText.text}: ", exception)
                        editText.error = Html.fromHtml("Failure on adding new groupId: <b>${editText.text}</b>.")
                        Utils.stopLoading(loadingDialog)
                    }
            }
        }
    }

    fun fetchGroups() {
        Utils.startLoading(loadingDialog)
        val email = DatabaseHelper.CURRENT_USER_EMAIL
        DatabaseHelper.getGroupsRaw(email)
            .addOnSuccessListener { documents ->
                if (!documents.isEmpty) {
                    Log.d(Utils.DEBUG_DB_GROUP, "${documents.size()} groups fetched with member: $email. Count: ${documents.size()}.")
                    groupsList = ArrayList()
                    for (group in documents) {
                        groupsList.add(DatabaseHelper.Group(group.id, group.data))
                    }
                    groupsList.sortWith(Comparator { group1: DatabaseHelper.Group, group2: DatabaseHelper.Group ->
                        when {
                            group1.name > group2.name -> 1
                            group1.name < group2.name -> -1
                            else -> 0
                        }
                    })
                    refreshAdapterData()
                } else {
                    Log.d(Utils.DEBUG_DB_GROUP, "Fetching groups with member $email: no records were found.")
                }
                Utils.stopLoading(loadingDialog)
            }
            .addOnFailureListener { exception ->
                Log.e(Utils.DEBUG_DB_GROUP, "Fail on fetching groups with member $email: ", exception)
                Utils.stopLoading(loadingDialog)
                Toast.makeText(context, getString(R.string.failed_to_fetch_groups), Toast.LENGTH_LONG).show()
            }
    }

    private fun refreshAdapterData() {
        adapter.expandableList = groupsList
        adapter.notifyDataSetChanged()
    }
}
