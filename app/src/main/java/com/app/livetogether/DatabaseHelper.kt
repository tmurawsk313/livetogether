package com.app.livetogether

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.Timestamp
import com.google.firebase.firestore.*
import java.util.*
import kotlin.collections.ArrayList

object DatabaseHelper {
    private const val TABLE_USERS = "users"
    private const val TABLE_GROUPS = "groups"
    private const val TABLE_TASKS = "tasks"

    var CURRENT_USER_EMAIL: String = ""

    private fun db() = FirebaseFirestore.getInstance()

    fun addUser(user: User) {
        db().collection(TABLE_USERS).document(user.email).get()
            .addOnSuccessListener { document ->
                if (!document.exists()) {
                    db().collection(TABLE_USERS).document(user.email).set(user.getDBO())
                        .addOnSuccessListener { docId ->
                            Log.d(Utils.DEBUG_DB_USER, "User created with ID: $docId")
                        }
                        .addOnFailureListener { exception ->
                            Log.e(Utils.DEBUG_DB_USER, "Error while adding User: ", exception)
                        }
                }
            }
            .addOnFailureListener { exception ->
                Log.e(Utils.DEBUG_DB_USER, "Fail on accessing User from database: ", exception)
            }
    }

    fun addUser(email: String, name: String, googleId: String) {
        addUser(User(email, name, googleId))
    }

    fun getUser(email: String): User? {
        val users: ArrayList<User> = ArrayList()
        db().collection(TABLE_USERS).document(email).get()
            .addOnSuccessListener { document ->
                if (document.exists()) {
                    Log.d(Utils.DEBUG_DB_USER, "User fetched: ${document.id}.")
                    users.add(User(document.data))
                } else {
                    Log.d(Utils.DEBUG_DB_USER, "User with such email does not exist: $email.")
                }
            }
            .addOnFailureListener { exception ->
                Log.e(Utils.DEBUG_DB_USER, "Fail on accessing User from database: ", exception)
            }
        return if (users.isEmpty()) null else users[0]
    }

    fun getUserRaw(email: String): Task<DocumentSnapshot> {
        return db().collection(TABLE_USERS).document(email).get()
    }

    fun getGroupsRaw(email: String): Task<QuerySnapshot> {
        return db().collection(TABLE_GROUPS).whereArrayContains(Group.MEMBERS, email).get()
    }

    fun removeGroupMember(group: Group, username: String) {
        db().collection(TABLE_GROUPS).document(group.id).update(Group.MEMBERS, FieldValue.arrayRemove(username))
            .addOnSuccessListener {
                Log.d(Utils.DEBUG_DB_GROUPMEMBER, "Successfully removed member $username from groupId ${group.name}.")
            }
            .addOnFailureListener { exception ->
                Log.e(
                    Utils.DEBUG_DB_GROUPMEMBER,
                    "Failure on removing member $username from groupId ${group.name}: ",
                    exception
                )
            }
    }

    fun addGroupMember(groupId: String, username: String) {
        db().collection(TABLE_GROUPS).document(groupId).update(Group.MEMBERS, FieldValue.arrayUnion(username))
    }

    fun addGroupRaw(name: String): Task<DocumentReference> {
        val newGroup = Group(
            name,
            CURRENT_USER_EMAIL,
            arrayListOf(CURRENT_USER_EMAIL),
            ""
        ) //TODO create google calendar & change "" to its id
        return db().collection(TABLE_GROUPS).add(newGroup.getDBO())
    }

    fun deleteGroup(group: Group) {
        db().collection(TABLE_GROUPS).document(group.id).delete()
            .addOnSuccessListener {
                Log.d(Utils.DEBUG_DB_GROUP, "Successfully removed groupId: ${group.name}.")
            }
            .addOnFailureListener { exception ->
                Log.e(Utils.DEBUG_DB_GROUP, "Failure on removing groupId: ${group.name}: ", exception)
            }
    }

    fun fetchTasksRaw(email: String): Query {
        return db().collection(TABLE_TASKS).whereEqualTo(CTask.ASSIGNEE, email).orderBy(CTask.START)
    }

    fun fetchAllTasksRaw(): Query {
        return db().collection(TABLE_TASKS).orderBy(CTask.START)
    }

    fun addTask(
        title: String,
        assignee: String,
        description: String,
        group: String,
        start: Date,
        end: Date
    ) {
        val newTask = CTask(
            "",
            title,
            assignee,
            description,
            group,
            Timestamp(start),
            Timestamp(end)
        )
        db().collection(TABLE_TASKS).add(newTask.getDBO())
            .addOnSuccessListener {
                Log.d(Utils.DEBUG_DB_TASK, "Successfully created task $title with id ${it.id} for user $assignee.")
            }
            .addOnFailureListener { exception ->
                Log.e(Utils.DEBUG_DB_TASK, "Failed on creating task $title for user $assignee: ", exception)
            }
    }

    fun updateTask(
        id: String,
        title: String,
        assignee: String,
        description: String,
        group: String,
        start: Date,
        end: Date
    ) {
        val newTask = CTask(
            id,
            title,
            assignee,
            description,
            group,
            Timestamp(start),
            Timestamp(end)
        )
        db().collection(TABLE_TASKS).document(id).update(newTask.getDBO())
            .addOnSuccessListener {
                Log.d(Utils.DEBUG_DB_TASK, "Successfully updated task $title with id $id for user $assignee.")
            }
            .addOnFailureListener { exception ->
                Log.e(Utils.DEBUG_DB_TASK, "Failed on updating task $title for user $assignee: ", exception)
            }
    }

    fun deleteTask(task: CTask) {
        db().collection(TABLE_TASKS).document(task.id).delete()
            .addOnSuccessListener {
                Log.d(
                    Utils.DEBUG_DB_TASK,
                    "Successfully deleted task ${task.title} with id ${task.id} for user ${task.assignee}."
                )
            }
            .addOnFailureListener { exception ->
                Log.e(
                    Utils.DEBUG_DB_TASK,
                    "Failed on deleting task ${task.title} with id ${task.id} for user ${task.assignee}: ",
                    exception
                )
            }
    }

    class User {
        var email: String
        var name: String
        var googleId: String

        companion object {
            const val EMAIL = "email"
            const val NAME = "name"
            const val GOOGLEID = "googleId"
        }

        constructor(email: String, name: String, googleId: String) {
            this.email = email
            this.name = name
            this.googleId = googleId
        }

        constructor(dbo: Map<String, Any>?) :
                this(
                    dbo!![EMAIL] as String,
                    dbo[NAME] as String,
                    dbo[GOOGLEID] as String
                )

        fun getDBO(): Map<String, Any> {
            return hashMapOf(
                EMAIL to this.email,
                NAME to this.name,
                GOOGLEID to this.googleId
            )
        }
    }

    class Group {
        var id: String = ""
        var name: String
        var admin: String
        var members: ArrayList<String>
        var calendarId: String

        companion object {
            const val NAME = "name"
            const val ADMIN = "admin"
            const val MEMBERS = "members"
            const val CALENDARID = "calendarId"
        }

        constructor(name: String, admin: String, members: ArrayList<String>, calendarId: String) : this(
            "",
            name,
            admin,
            members,
            calendarId
        )

        constructor(id: String, name: String, admin: String, members: ArrayList<String>, calendarId: String) {
            this.id = id
            this.name = name
            this.admin = admin
            this.members = members
            this.calendarId = calendarId
        }

        constructor(id: String, dbo: Map<String, Any>?) :
                this(
                    id,
                    dbo!![NAME] as String,
                    dbo[ADMIN] as String,
                    dbo[MEMBERS] as ArrayList<String>,
                    dbo[CALENDARID] as String
                )

        fun getDBO(): Map<String, Any> {
            return hashMapOf(
                NAME to this.name,
                ADMIN to this.admin,
                MEMBERS to this.members,
                CALENDARID to this.calendarId
            )
        }
    }

    class GroupWithTasks(var group: Group, var tasks: ArrayList<CTask> = ArrayList())

    class CTask {
        var id: String
        var title: String
        var assignee: String
        var description: String
        var groupId: String
        var start: Date
        var end: Date

        companion object {
            const val TITLE = "title"
            const val ASSIGNEE = "assignee"
            const val GROUPID = "groupId"
            const val START = "start"
            const val END = "end"
            const val DESCRIPTION = "description"
        }

        constructor(
            id: String,
            title: String,
            assignee: String,
            description: String,
            group: String,
            start: Timestamp,
            end: Timestamp
        ) {
            this.id = id
            this.title = title
            this.assignee = assignee
            this.description = description
            this.groupId = group
            this.start = start.toDate()
            this.end = end.toDate()
        }

        constructor(id: String, dbo: Map<String, Any>?) :
                this(
                    id,
                    dbo!![TITLE] as String,
                    dbo[ASSIGNEE] as String,
                    dbo[DESCRIPTION] as String,
                    dbo[GROUPID] as String,
                    dbo[START] as Timestamp,
                    dbo[END] as Timestamp
                )

        fun getDBO(): Map<String, Any> {
            return hashMapOf(
                TITLE to this.title,
                ASSIGNEE to this.assignee,
                DESCRIPTION to this.description,
                GROUPID to this.groupId,
                START to this.start,
                END to this.end
            )
        }
    }
}