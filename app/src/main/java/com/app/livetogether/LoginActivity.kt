package com.app.livetogether

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    companion object {
        const val RC_SIGN_IN: Int = 666
        lateinit var signInOptions: GoogleSignInOptions
    }

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()
        setupGoogleSignIn()
    }

    private fun setupGoogleSignIn() {
        signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        val mGoogleSignInClient = GoogleSignIn.getClient(this, signInOptions)
        signInButton.setOnClickListener {
            startActivityForResult(mGoogleSignInClient.signInIntent, RC_SIGN_IN)
        }
    }

    override fun onStart() {
        super.onStart()
        val currentGoogleUser = auth.currentUser
        if (currentGoogleUser != null) {
            if (currentGoogleUser.email != null) {
                val user = DatabaseHelper.getUser(currentGoogleUser.email!!)
                if (user != null)
                    DatabaseHelper.addUser(
                        currentGoogleUser.email as String,
                        currentGoogleUser.displayName as String,
                        currentGoogleUser.uid
                    )
            }
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        DatabaseHelper.CURRENT_USER_EMAIL = auth.currentUser!!.email!!
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN && data != null) {
            try {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                val account: GoogleSignInAccount? = task.getResult(ApiException::class.java)
                authenticateWithFirebase(account!!)
            } catch (e: ApiException) {
                Log.e(
                    Utils.DEBUG_GOOGLE_AUTH,
                    "ERROR! Status code: " + e.statusCode + "\n\tMessage: " + e.message + "\n\tLocalized message: " + e.localizedMessage
                )
            } catch (e: Exception) {
                Log.e(
                    Utils.DEBUG_GOOGLE_AUTH,
                    "ERROR!\n\tMessage: " + e.message + "\n\tLocalized message: " + e.localizedMessage
                )
            }
        }
    }

    private fun authenticateWithFirebase(account: GoogleSignInAccount) {
        Log.d(Utils.DEBUG_FIREBASE, "Firebase authentication - account ID: " + account.id)
        Log.d(Utils.DEBUG_FIREBASE, "Firebase authentication - account name: " + account.displayName)
        Log.d(Utils.DEBUG_FIREBASE, "Firebase authentication - account email: " + account.email)

        val credential = GoogleAuthProvider.getCredential(account.idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d(Utils.DEBUG_FIREBASE, "Firebase authentication with credentials: SUCCESS")
                    DatabaseHelper.addUser(DatabaseHelper.User(account.email!!, account.displayName!!, account.id!!))
                    finish()
                } else {
                    Log.e(Utils.DEBUG_FIREBASE, "Firebase authentication with credentials: FAILURE", task.exception)
                    Snackbar.make(
                        findViewById<View>(android.R.id.content),
                        getString(R.string.msg_authFailed),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            }
    }
}
