package com.app.livetogether

import android.app.ProgressDialog
import android.content.Intent
import android.support.v4.app.Fragment
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import android.widget.AdapterView
import android.widget.Toast
import android.widget.AdapterView.OnItemSelectedListener
import com.google.firebase.Timestamp
import kotlinx.android.synthetic.main.fragment_tasks.*


class TasksFragment : Fragment() {
    lateinit var myView: View
    lateinit var adapter: TasksExpandableListAdapter
    lateinit var tasksList: ArrayList<DatabaseHelper.GroupWithTasks>
    lateinit var loadingDialog: ProgressDialog
    lateinit var viewTypeAdapter: ArrayAdapter<String>

    companion object {
        fun newInstance(): TasksFragment {
            return TasksFragment()
        }
        var editTask = DatabaseHelper.CTask("null", "", "", "", "", Timestamp.now(), Timestamp.now())

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        myView = inflater.inflate(R.layout.fragment_tasks, container, false)
        adapter = TasksExpandableListAdapter(this, context!!, ArrayList())
        loadingDialog = Utils.setupLoadingDialog(context!!)
        val groupsListView = myView.findViewById<ExpandableListView>(R.id.tasksListView)
        groupsListView.setAdapter(adapter)
        groupsListView.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
            editTask = tasksList[groupPosition].tasks[childPosition]
            startActivity(Intent(context, NewTaskActivity::class.java))
            true
        }

        val tasksViewTypePicklist = myView.findViewById<Spinner>(R.id.tasksListType)
        viewTypeAdapter = ArrayAdapter(
            context!!,
            android.R.layout.simple_spinner_dropdown_item,
            arrayListOf(getString(R.string.tvt_all_tasks), getString(R.string.tvt_by_groups))
        )
        tasksViewTypePicklist.adapter = viewTypeAdapter
        tasksViewTypePicklist.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                arg2: Int, arg3: Long
            ) {
                fetchGroupsWithTasks(myView)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        myView.findViewById<CheckBox>(R.id.tasksAllCheckbox).setOnCheckedChangeListener { _, _ ->
            fetchGroupsWithTasks(myView)
        }
        fetchGroups(myView)

        myView.findViewById<FloatingActionButton>(R.id.addTaskButton).setOnClickListener {
            startActivity(Intent(context, NewTaskActivity::class.java))
        }

        return myView
    }

    private fun fetchGroups(v: View) {
        Utils.startLoading(loadingDialog)
        val email = DatabaseHelper.CURRENT_USER_EMAIL
        DatabaseHelper.getGroupsRaw(email)
            .addOnSuccessListener { fetchedGroups ->
                Log.d(Utils.DEBUG_DB_GROUP, "Successfully fetched ${fetchedGroups.size()} groups for user: $email")
                fetchedGroups.forEach { group ->
                    viewTypeAdapter.add(DatabaseHelper.Group(group.id, group.data).name)
                }
                viewTypeAdapter.notifyDataSetChanged()
                fetchGroupsWithTasks(v)
            }
            .addOnFailureListener { exception ->
                Log.e(Utils.DEBUG_DB_GROUP, "Failed to fetch groups for user $email: ", exception)
                Utils.stopLoading(loadingDialog)
            }
    }

    fun fetchGroupsWithTasks(v: View) {
        Utils.startLoading(loadingDialog)
        val email = DatabaseHelper.CURRENT_USER_EMAIL
        val selectedViewType = v.findViewById<Spinner>(R.id.tasksListType).selectedItem.toString()
        val isAllTasks = v.findViewById<CheckBox>(R.id.tasksAllCheckbox).isChecked
        DatabaseHelper.getGroupsRaw(email)
            .addOnSuccessListener { fetchedGroups ->
                Log.d(Utils.DEBUG_DB_GROUP, "Successfully fetched ${fetchedGroups.size()} groups for user: $email")
                if (!fetchedGroups.isEmpty) {
                    val groupsByIds = HashMap<String, DatabaseHelper.Group>()
                    if (selectedViewType == getString(R.string.tvt_all_tasks) || selectedViewType.isBlank()) {
                        groupsByIds[getString(R.string.tvt_all_tasks)] =
                            DatabaseHelper.Group(getString(R.string.all), email, ArrayList(), "")
                    } else {
                        fetchedGroups.forEach { group ->
                            val myGroup = DatabaseHelper.Group(group.id, group.data)
                            if (selectedViewType == getString(R.string.tvt_by_groups)
                                || selectedViewType == myGroup.name
                            )
                                groupsByIds[group.id] = myGroup
                        }
                    }
                    val query =
                        if (isAllTasks)
                            DatabaseHelper.fetchAllTasksRaw()
                        else
                            DatabaseHelper.fetchTasksRaw(email)

                    query.get()
                        .addOnSuccessListener { fetchedTasks ->
                            Log.d(Utils.DEBUG_DB_TASK, "Successfully fetched ${fetchedTasks.size()} tasks for user: $email.")
                            tasksList = ArrayList()
                            val tasksByGroupId = HashMap<String, ArrayList<DatabaseHelper.CTask>>()
                            groupsByIds.keys.forEach { id -> tasksByGroupId[id] = ArrayList() }

                            fetchedTasks.forEach { task ->
                                val cTask = DatabaseHelper.CTask(task.id, task.data)
                                if (selectedViewType == getString(R.string.tvt_all_tasks))
                                    tasksByGroupId[getString(R.string.tvt_all_tasks)]?.add(cTask)
                                else
                                    tasksByGroupId[cTask.groupId]?.add(cTask)
                            }

                            groupsByIds.keys.forEach { groupId ->
                                tasksList.add(
                                    DatabaseHelper.GroupWithTasks(
                                        groupsByIds[groupId]!!,
                                        tasksByGroupId[groupId]!!
                                    )
                                )
                                adapter.expandableList = tasksList
                                adapter.notifyDataSetChanged()
                            }
                            Utils.stopLoading(loadingDialog)
                        }
                        .addOnFailureListener { exception ->
                            Log.e(Utils.DEBUG_DB_TASK, "Failed to fetch tasks for user $email: ", exception)
                            Utils.stopLoading(loadingDialog)
                            Toast.makeText(context, getString(R.string.failed_to_fetch_tasks), Toast.LENGTH_LONG).show()
                        }
                }
                Utils.stopLoading(loadingDialog)
            }
            .addOnFailureListener { exception ->
                Log.e(Utils.DEBUG_DB_GROUP, "Failed to fetch groups for user $email: ", exception)
                Utils.stopLoading(loadingDialog)
            }
    }
}