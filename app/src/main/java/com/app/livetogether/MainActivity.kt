package com.app.livetogether

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        var DISPLAYED_TEXT: Boolean = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.itemIconTintList = null;

        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, TasksFragment.newInstance())
        transaction.commit()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        var selectedFragment: Fragment = TasksFragment.newInstance()
        when (item.itemId) {
            R.id.navigation_tasks -> {
                selectedFragment = TasksFragment.newInstance()
            }
            R.id.navigation_groups -> {
                selectedFragment = GroupsFragment.newInstance()
            }
            R.id.navigation_notifications -> {
                selectedFragment = NotificationsFragment.newInstance()
            }
        }
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, selectedFragment)
        transaction.commit()
        true
    }

    override fun onStart() {
        super.onStart()
        val auth = FirebaseAuth.getInstance()
        if (auth.currentUser == null) {
            startActivity(Intent(this, LoginActivity::class.java))
        } else if (!DISPLAYED_TEXT) {
            DatabaseHelper.CURRENT_USER_EMAIL = auth.currentUser!!.email!!
            Toast.makeText(
                this,
                getString(R.string.logged_in_as) + " " + auth.currentUser!!.displayName,
                Toast.LENGTH_LONG
            ).show()
            DISPLAYED_TEXT = true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.settings, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_settings) {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item);
    }
}
