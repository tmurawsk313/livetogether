package com.app.livetogether

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context

object Utils {
    const val DEBUG_DB_USER = "DB_USER"
    const val DEBUG_DB_GROUP = "DB_GROUP"
    const val DEBUG_DB_GROUPMEMBER = "DB_GROUPMEMBER"
    const val DEBUG_DB_TASK = "DB_TASK"
    const val DEBUG_GOOGLE_AUTH = "GOOGLE_AUTH"
    const val DEBUG_FIREBASE = "FIREBASE"

    fun setupLoadingDialog(context: Context): ProgressDialog {
        val loadingDialog = ProgressDialog(context)
        loadingDialog.setTitle(context.getString(R.string.loading))
        loadingDialog.setMessage(context.getString(R.string.please_wait))
        loadingDialog.setCancelable(false)
        return loadingDialog
    }

    fun startLoading(loadingDialog: AlertDialog) {
        if (!loadingDialog.isShowing) {
            loadingDialog.show()
        }
    }

    fun stopLoading(loadingDialog: AlertDialog) {
        if (loadingDialog.isShowing) {
            loadingDialog.dismiss()
        }
    }
}