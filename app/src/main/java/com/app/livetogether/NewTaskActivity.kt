package com.app.livetogether

import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.app.TimePickerDialog
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import com.google.firebase.Timestamp
import kotlinx.android.synthetic.main.activity_new_task.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class NewTaskActivity() : AppCompatActivity() {

    private val booleanToVisibility = hashMapOf(
        true to View.VISIBLE,
        false to View.INVISIBLE
    )

    lateinit var groupsAdapter: ArrayAdapter<String>
    lateinit var assigneeAdapter: ArrayAdapter<String>
    lateinit var userGroups: HashMap<String, DatabaseHelper.Group>
    lateinit var loadingDialog: ProgressDialog
    lateinit var editTask: DatabaseHelper.CTask
    var isEdit = false

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_task)
        if (TasksFragment.editTask.id != "null") {
            this.editTask = TasksFragment.editTask
            this.isEdit = true
        }

        userGroups = HashMap()
        loadingDialog = Utils.setupLoadingDialog(this)
        Utils.startLoading(loadingDialog)

        repeatedLayout.visibility = booleanToVisibility[isRepeatedCheckbox.isChecked]!!
        isRepeatedCheckbox.setOnCheckedChangeListener { _, isChecked ->
            repeatedLayout.visibility = booleanToVisibility[isChecked]!!
        }

        setupButtons()
        setupDateTimeFields()

        if (!isEdit) {
            fetchGroups()
        } else {
            groupPicklist.visibility = View.GONE
            groupTextView.visibility = View.GONE
            assigneePicklist.visibility = View.GONE
            assigneeTextView.visibility = View.GONE
            isRepeatedCheckbox.visibility = View.GONE
            nameEditText.setText(editTask.title)
            descriptionEditText.setText(editTask.description)
            startDateEditText.setText(SimpleDateFormat("dd.MM.yyyy").format(editTask.start))
            endDateEditText.setText(SimpleDateFormat("dd.MM.yyyy").format(editTask.end))
            startTimeEditText.setText(
                SimpleDateFormat("HH:mm").format(
                    Date(
                        1,
                        1,
                        1,
                        editTask.start.hours,
                        editTask.start.minutes
                    )
                )
            )
            endTimeEditText.setText(
                SimpleDateFormat("HH:mm").format(
                    Date(
                        1,
                        1,
                        1,
                        editTask.end.hours,
                        editTask.end.minutes
                    )
                )
            )
            Utils.stopLoading(loadingDialog)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        TasksFragment.editTask = DatabaseHelper.CTask("null", "", "", "", "", Timestamp.now(), Timestamp.now())
    }

    private fun setupButtons() {
        cancelButton.setOnClickListener {
            finish()
        }
        saveButton.setOnClickListener {
            if (validateInput()) {
                if (isEdit) {
                    updateTask()
                } else {
                    createTasks()
                }
                finish()
            }
        }
    }

    private fun updateTask() {
        DatabaseHelper.updateTask(
            editTask.id,
            nameEditText.text.toString(),
            editTask.assignee,
            descriptionEditText.text.toString(),
            editTask.groupId,
            getStartDateTimeForCalendar(),
            getEndDateTimeForCalendar()
        )
    }

    private fun createTasks() {
        var howMany = 0
        var interval = 0
        if (isRepeatedCheckbox.isChecked) {
            howMany = timesEditText.text.toString().toInt()
            interval = daysEditText.text.toString().toInt()
        }
        val assigneeCount = assigneePicklist.count
        var assigneeIndex = assigneePicklist.selectedItemPosition

        for (i in 0..howMany) {
            val startDate = Calendar.getInstance()
            startDate.time = getStartDateTimeForCalendar()
            startDate.add(Calendar.DATE, i * interval)

            val endDate = Calendar.getInstance()
            endDate.time = getEndDateTimeForCalendar()
            endDate.add(Calendar.DATE, i * interval)

            val assignee = assigneePicklist.getItemAtPosition(assigneeIndex++%assigneeCount)

            DatabaseHelper.addTask(
                nameEditText.text.toString(),
                assignee as String,
                descriptionEditText.text.toString(),
                userGroups[groupPicklist.selectedItem.toString()]!!.id,
                startDate.time,
                endDate.time
            )
        }
    }

    private fun setupPicklists(groups: ArrayList<String>, assignees: ArrayList<String>) {
        assigneeAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, assignees)
        assigneePicklist.adapter = assigneeAdapter

        groupsAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, groups)
        groupPicklist.adapter = groupsAdapter
        groupPicklist.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                assigneeAdapter.clear()
                assigneeAdapter.addAll(userGroups[groupsAdapter.getItem(position)!!]!!.members)
                assigneeAdapter.notifyDataSetChanged()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun fetchGroups() {
        val email = DatabaseHelper.CURRENT_USER_EMAIL
        DatabaseHelper.getGroupsRaw(email)
            .addOnSuccessListener { fetchedGroups ->
                Log.d(Utils.DEBUG_DB_GROUP, "Successfully fetched ${fetchedGroups.size()} groups for user $email.")
                val groupNames = ArrayList<String>()
                fetchedGroups.forEach { fetchedGroup ->
                    val group = DatabaseHelper.Group(fetchedGroup.id, fetchedGroup.data)
                    userGroups[group.name] = group
                    groupNames.add(group.name)
                }
                setupPicklists(groupNames, userGroups[groupNames[0]]!!.members)
                Utils.stopLoading(loadingDialog)
            }
            .addOnFailureListener { exception ->
                Log.e(Utils.DEBUG_DB_GROUP, "Failed on fetching groups for user $email: ", exception)
                Utils.stopLoading(loadingDialog)
            }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun setupDateTimeFields() {
        setupDateField(startDateEditText)
        setupDateField(endDateEditText)
        setupTimeField(startTimeEditText)
        setupTimeField(endTimeEditText)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun setupDateField(editText: EditText) {
        editText.setText(DateFormat.getDateInstance().format(Date()))
        editText.setOnClickListener {
            val dialog = DatePickerDialog(this)
            dialog.setOnDateSetListener { _, year, month, dayOfMonth ->
                editText.setText(DateFormat.getDateInstance().format(Date(year - 1900, month, dayOfMonth)))
            }
            dialog.show()
        }
    }

    private fun setupTimeField(editText: EditText) {
        val currDate = Date()
        editText.setText(SimpleDateFormat("HH:mm").format(currDate))
        editText.setOnClickListener {
            TimePickerDialog(
                this,
                TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                    editText.setText(SimpleDateFormat("HH:mm").format(Date(1, 1, 1, hourOfDay, minute)))
                },
                currDate.hours,
                currDate.minutes,
                true
            ).show()
        }
    }

    private fun validateInput(): Boolean {
        var isValid = true

        val startDate = getStartDateTime()
        val endDate = getEndDateTime()

        if (nameEditText.text.isBlank()) {
            nameEditText.error = getString(R.string.field_cant_be_empty)
            isValid = false
        }
        if (!isEdit && (groupPicklist.selectedItem == null || (groupPicklist.selectedItem as String).isBlank())) {
            groupTextView.error = getString(R.string.group_must_be_specified)
            isValid = false
        }
        if (!isEdit && (assigneePicklist.selectedItem == null || (assigneePicklist.selectedItem as String).isBlank())) {
            assigneeTextView.error = getString(R.string.assignee_must_be_specified)
            isValid = false
        }
        if (startDate >= endDate) {
            endDateEditText.error = getString(R.string.end_date_greater_than_start_date)
            endTimeEditText.error = getString(R.string.end_date_greater_than_start_date)
            isValid = false
        }
        if (startDate < Date()) {
            startDateEditText.error = getString(R.string.task_cannot_start_in_past)
            isValid = false
        }
        if (isRepeatedCheckbox.isChecked && timesEditText.text.toString().isBlank()) {
            timesEditText.error = getString(R.string.field_cant_be_empty)
            isValid = false
        }
        if (isRepeatedCheckbox.isChecked && daysEditText.text.toString().isBlank()) {
            daysEditText.error = getString(R.string.field_cant_be_empty)
            isValid = false
        }

        return isValid
    }

    private fun getStartDateTime(): Date {
        val dateFrags = startDateEditText.text.toString().split(".")
        val timeFrags = startTimeEditText.text.toString().split(":")
        return Date(
            dateFrags[2].toInt() - 1900,
            dateFrags[1].toInt(),
            dateFrags[0].toInt(),
            timeFrags[0].toInt(),
            timeFrags[1].toInt()
        )
    }

    private fun getStartDateTimeForCalendar(): Date {
        val dateFrags = startDateEditText.text.toString().split(".")
        val timeFrags = startTimeEditText.text.toString().split(":")
        return Date(
            dateFrags[2].toInt() - 1900,
            dateFrags[1].toInt() - 1,
            dateFrags[0].toInt(),
            timeFrags[0].toInt(),
            timeFrags[1].toInt()
        )
    }

    private fun getEndDateTime(): Date {
        val dateFrags = endDateEditText.text.toString().split(".")
        val timeFrags = endTimeEditText.text.toString().split(":")
        return Date(
            dateFrags[2].toInt() - 1900,
            dateFrags[1].toInt(),
            dateFrags[0].toInt(),
            timeFrags[0].toInt(),
            timeFrags[1].toInt()
        )
    }

    private fun getEndDateTimeForCalendar(): Date {
        val dateFrags = endDateEditText.text.toString().split(".")
        val timeFrags = endTimeEditText.text.toString().split(":")
        return Date(
            dateFrags[2].toInt() - 1900,
            dateFrags[1].toInt() - 1,
            dateFrags[0].toInt(),
            timeFrags[0].toInt(),
            timeFrags[1].toInt()
        )
    }
}
