package com.app.livetogether

import android.os.Bundle
import android.preference.Preference
import android.preference.PreferenceFragment
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth

class SettingsFragment : PreferenceFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.pref_general)
        setupLogoutButton()
    }

    private fun setupLogoutButton() {
        val logoutButton: Preference = findPreference(getString(R.string.pref_logout))
        logoutButton.setOnPreferenceClickListener {
            MainActivity.DISPLAYED_TEXT = false
            FirebaseAuth.getInstance().signOut()
            GoogleSignIn.getClient(activity, GoogleSignInOptions.DEFAULT_SIGN_IN).signOut()
            this.activity?.finishAffinity()
            true
        }
    }
}