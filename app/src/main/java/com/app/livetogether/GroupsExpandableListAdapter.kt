package com.app.livetogether

import android.app.AlertDialog
import android.content.Context
import android.graphics.Typeface
import android.text.Html
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.EditText
import kotlinx.android.synthetic.main.groups_item_child.view.*
import kotlinx.android.synthetic.main.groups_item_parent.view.*


class GroupsExpandableListAdapter() : BaseExpandableListAdapter() {

    private lateinit var context: Context
    private lateinit var parent: GroupsFragment
    lateinit var expandableList: List<DatabaseHelper.Group>

    constructor(parent: GroupsFragment, context: Context, expandableList: List<DatabaseHelper.Group>) : this() {
        this.context = context
        this.parent = parent
        this.expandableList = expandableList
    }

    override fun getChild(listPosition: Int, expandedListPosition: Int): Any {
        return expandableList[listPosition].members[expandedListPosition]
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(
        listPosition: Int,
        expandedListPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var myConvertView = convertView
        val expandedListText = getChild(listPosition, expandedListPosition) as String
        if (myConvertView == null) {
            val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            myConvertView = layoutInflater.inflate(R.layout.groups_item_child, null)
        }
        myConvertView!!.memberName.text = expandedListText
        val group = getGroup(listPosition) as DatabaseHelper.Group
        if (group.admin != DatabaseHelper.CURRENT_USER_EMAIL || expandedListText == group.admin) {
            myConvertView.deleteButton.visibility = View.GONE
        } else {
            myConvertView.deleteButton.setOnClickListener {
                onDeleteMemberButtonClicked(group, expandedListText)
            }
        }
        return myConvertView
    }

    override fun getChildrenCount(listPosition: Int): Int {
        return expandableList[listPosition].members.size
    }

    override fun getGroup(listPosition: Int): Any {
        return expandableList[listPosition]
    }

    override fun getGroupCount(): Int {
        return expandableList.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getGroupView(
        listPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var myConvertView = convertView
        val currentGroup = getGroup(listPosition) as DatabaseHelper.Group
        if (myConvertView == null) {
            val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            myConvertView = layoutInflater.inflate(R.layout.groups_item_parent, null)
        }
        myConvertView!!.groupName.setTypeface(null, Typeface.BOLD)
        myConvertView.groupName.text = currentGroup.name

        if (currentGroup.admin != DatabaseHelper.CURRENT_USER_EMAIL) {
            myConvertView.addMemberButton.visibility = View.GONE
            myConvertView.deleteGroupButton.visibility = View.GONE
        } else {
            myConvertView.addMemberButton.setOnClickListener {
                onAddMemberButtonClicked(currentGroup)
            }
            myConvertView.deleteGroupButton.setOnClickListener {
                onDeleteGroupButtonClicked(currentGroup)
            }
        }

        return myConvertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        return true
    }

    private fun onDeleteMemberButtonClicked(group: DatabaseHelper.Group, username: String) {
        AlertDialog.Builder(context)
            .setTitle(R.string.delete_group_member)
            .setIcon(R.drawable.ic_remove_user)
            .setNegativeButton(R.string.No) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(R.string.Yes) { dialog, _ ->
                DatabaseHelper.removeGroupMember(group, username)
                parent.fetchGroups()
                dialog.dismiss()
            }
            .setMessage(Html.fromHtml("Are you sure you want to delete user <b>$username</b> from the groupId <b>${group.name}</b>?"))
            .show()
    }

    private fun onAddMemberButtonClicked(group: DatabaseHelper.Group) {
        val editText = EditText(context)
        editText.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS

        val dialog = AlertDialog.Builder(context)
            .setView(editText)
            .setTitle(R.string.add_group_member)
            .setIcon(R.drawable.ic_add_user)
            .setNegativeButton(R.string.Cancel) { _, _ -> }
            .setPositiveButton(R.string.Confirm) { _, _ -> }
            .show()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (group.members.contains(editText.text.toString())) {
                editText.error =
                    Html.fromHtml("User with name <b>${editText.text}</b> is already in groupId <b>${group.name}</b>.")
            } else {
                Utils.startLoading(parent.loadingDialog)
                DatabaseHelper.getUserRaw(editText.text.toString())
                    .addOnSuccessListener {
                        if (it.exists()) {
                            Log.d(Utils.DEBUG_DB_USER, "User fetched: ${it.id}.")
                            DatabaseHelper.addGroupMember(group.id, editText.text.toString())
                            parent.fetchGroups()
                            dialog.dismiss()
                        } else {
                            Log.w(Utils.DEBUG_DB_USER, "User with email ${editText.text} does not exist.")
                            editText.error = Html.fromHtml("User with name <b>${editText.text}</b> does not exist.")
                        }
                        Utils.stopLoading(parent.loadingDialog)
                    }
                    .addOnFailureListener {
                        Log.e(Utils.DEBUG_DB_USER, "User failed to fetch: ${editText.text}.")
                        editText.error = "Unknown error occurred. Please check your Internet connection and try again."
                        Utils.stopLoading(parent.loadingDialog)
                    }
            }
        }
    }

    private fun onDeleteGroupButtonClicked(group: DatabaseHelper.Group) {
        AlertDialog.Builder(context)
            .setTitle(R.string.delete_group)
            .setIcon(R.drawable.ic_remove_group)
            .setNegativeButton(R.string.No) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(R.string.Yes) { dialog, _ ->
                DatabaseHelper.deleteGroup(group)
                parent.fetchGroups()
                dialog.dismiss()
            }
            .setMessage(Html.fromHtml("Are you sure you want to delete group <b>${group.name}</b>?"))
            .show()
    }
}
