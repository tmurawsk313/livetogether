package com.app.livetogether

import android.app.AlertDialog
import android.content.Context
import android.graphics.Typeface
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.CheckBox
import android.widget.Spinner
import kotlinx.android.synthetic.main.tasks_item_child.view.*
import kotlinx.android.synthetic.main.tasks_item_parent.view.*
import java.text.SimpleDateFormat


class TasksExpandableListAdapter() : BaseExpandableListAdapter() {

    private lateinit var context: Context
    private lateinit var parent: TasksFragment
    lateinit var expandableList: List<DatabaseHelper.GroupWithTasks>

    constructor(parent: TasksFragment, context: Context, expandableList: List<DatabaseHelper.GroupWithTasks>) : this() {
        this.context = context
        this.parent = parent
        this.expandableList = expandableList
    }

    override fun getChild(listPosition: Int, expandedListPosition: Int): Any {
        return expandableList[listPosition].tasks[expandedListPosition]
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(
        listPosition: Int,
        expandedListPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var myConvertView = convertView
        val task = getChild(listPosition, expandedListPosition) as DatabaseHelper.CTask
        if (myConvertView == null) {
            val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            myConvertView = layoutInflater.inflate(R.layout.tasks_item_child, null)
        }
        myConvertView!!.taskTitle.text = task.title

        val tasksListType = this.parent.myView.findViewById<Spinner>(R.id.tasksListType)
        val allTasksOption = this.parent.myView.findViewById<CheckBox>(R.id.tasksAllCheckbox)
        val startDateString = SimpleDateFormat("dd.MM.yyyy HH:mm").format(task.start)
        val endDateString = SimpleDateFormat("dd.MM.yyyy HH:mm").format(task.end)

        if (tasksListType.selectedItem == null || tasksListType.selectedItem == parent.context.getString(R.string.all_tasks)) {
            if (allTasksOption.isChecked) {
                myConvertView.taskInfo.text = "${task.assignee} ($startDateString)"
            } else {
                myConvertView.taskInfo.text = "$startDateString - $endDateString"
            }
        } else {
            if (allTasksOption.isChecked) {
                myConvertView.taskInfo.text = "${task.assignee} ($startDateString - $endDateString)"
            } else {
                myConvertView.taskInfo.text = "$startDateString - $endDateString"
            }
        }
        if (task.assignee != DatabaseHelper.CURRENT_USER_EMAIL) {
            myConvertView.deleteButton.visibility = View.GONE
        } else {
            myConvertView.deleteButton.setOnClickListener {
                onCompleteTaskButtonClicked(task)
            }
        }
        return myConvertView
    }

    private fun onCompleteTaskButtonClicked(task: DatabaseHelper.CTask) {
        AlertDialog.Builder(context)
            .setTitle(R.string.complete_task)
            .setIcon(R.drawable.ic_checked)
            .setNegativeButton(R.string.No) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(R.string.Yes) { dialog, _ ->
                DatabaseHelper.deleteTask(task)
                parent.fetchGroupsWithTasks(parent.myView)
                dialog.dismiss()
            }
            .setMessage(Html.fromHtml("Are you sure you want to complete task <b>${task.title}</b>?"))
            .show()
    }

    override fun getChildrenCount(listPosition: Int): Int {
        return expandableList[listPosition].tasks.size
    }

    override fun getGroup(listPosition: Int): Any {
        return expandableList[listPosition]
    }

    override fun getGroupCount(): Int {
        return expandableList.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getGroupView(
        listPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var myConvertView = convertView
        val currentGroup = getGroup(listPosition) as DatabaseHelper.GroupWithTasks
        if (myConvertView == null) {
            val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            myConvertView = layoutInflater.inflate(R.layout.tasks_item_parent, null)
        }
        myConvertView!!.groupNameTasks.setTypeface(null, Typeface.BOLD)
        myConvertView.groupNameTasks.text = currentGroup.group.name
        
        return myConvertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        return true
    }
}